#!/bin/bash
# SCRIPT		:	rocknplay.sh
# WRITTEN BY	:	Daniele Antonucci
# PURPOSE		:	Simple frontend for Rock64 and Debian Stretch to run games
# 					which has following options:
# 					Run RetroArch through FlatPak;
# 					Run in Debian Emulators;
#					Run GNU Games;
#					Run Netsurf Browser;
# 					Exit.
#
#
# The MIT License (MIT)
#
# Copyright (c) 2018 Daniele Antonucci
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

################################################################################
#                                                                              #
#                                COMMON VARIABLES                              #
#                                                                              #
################################################################################

LOGO="
    ██████▄                   ███      \Z1██\Zn          \Z1██\Zn ▄█████▄   ███
   ▐███ ▐██▌  ▗▄▄▄     ▗▄▄▖  ███▌ ▗▄█ \Z1▐█\Zn  █▙▄ ▄▄  \Z1▐█\Zn  ███  ██▌ ███   ▄▄▄   ▄▄  ▄▄
   ███████▀ ███▀▜██▌ ███▀██▌ ███ ██▀     ███▀███▌    ███▌ ███ ▐██▌ ▀▀  ▜█▌ ██  █▀
  ▐███ ███ ▐██▌ ███▌███▌ ▄▄  ██████     ▐███ ███    ▗███▀▀▀   ███ ▟█▛▀▀▜█▀ ▜█▄█▛
  ███▀ ▐███ ▀████▀▘  ▀████▀ ███  ███   ▐███ ███     ███      ███  ██▄▗▄██▙  ██▛
                                                                          ▗█▀"


################################################################################
#                                                                              #
#                                  SUB MENUS                                   #
#                                                                              #
################################################################################

function  submenu1
{

	#
	# set infinite loop
	#

   while true
	 do
		 dialog  --begin 4 10  --colors --backtitle "Rock'n'Play" \
		 --no-lines --no-collapse --no-shadow --infobox  "$LOGO" 12 88 \
		 --and-widget  --begin 18 10 --nocancel --no-lines  \
		 --title "Test submenu and Rock!" \
		 --menu " " 15 88 15 \
		 1 "Test01" \
		 2 "test02"	\
		 3 "Test03"  2>/tmp/menuitem.$$

		 menuitem=`cat /tmp/menuitem.$$`

		 opt=$?

		 # make decsion
		 case $menuitem in
		 	1) echo "ONE"  ; break ;;
		 	2) echo "TWO"  ; break ;;
		 	3) echo "THREE"  ; break ;;
		 esac

		 done
 }



 ###############################################################################
 #                                                                             #
 #                                   MAIN MENU                                 #
 #                                                                             #
 ###############################################################################

#
# set infinite loop
#

while true
do

dialog  --begin 4 10  --colors --backtitle "Rock'n'Play" --no-lines \
--no-collapse --no-shadow --infobox  "$LOGO" 12 88 \
--and-widget  --begin 18 10 --keep-window --no-shadow --nocancel \
--no-lines --ok-button "Run" \
--title "Choose an option and Rock!" \
--menu " " 15 88 15 \
1 "Exit" \
2 "RetroArch" \
3 "SubMenu Test" \
4 "Web Browser" \
5 "Reload" \
6 "Restart" \
7 "Shutdown" 2>/tmp/menuitem.$$

menuitem=`cat /tmp/menuitem.$$`

opt=$?

# make decsion
case $menuitem in
    1) echo "bye" ; break ;;
	2) /usr/bin/startx retroarch ;;
	3) submenu1 ;;
	4) /usr/bin/startx /usr/bin/netsurf --window_width 1920 --window_height 1080 ;;
	5) /Users/daniel/./rocknplay.sh  ;;
	6) /bin/systemctl reboot -i ;;
	7) /bin/systemctl poweroff -i ;;
esac

done
