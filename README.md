## Introduction

Rock'n'Play is a minimalist but yet powerfull shell frontend to run 
emulators and games on Debian Minimal, it is mostly tailored around the 
Pine64 single board Rock64 but you can easily adapt it to whatever 
board or distro. It makes use of Dialog to create the shell widgets and 
it can run an X session at once through Xinit or StartX without DE or 
WM. The main difference between Lakka or Recalbox is the fact that, 
under the hood, is a Debian system so it behaves as full GNU/Linux system.


## What it should be able to:

* run Retroarch as flatpak application;
* run GNU games like SuperTux Kart;
* run a web-broswer like NetSurf (or Midori);
* restart, Shutdown and exit.


## Requirements 

If you are testing in a full Debian with a DE or a WM maybe you just 
need ```dialog```. If you are testing in a Debian minimal you need 
these packages[^1] installed:

* dialog
* flatpak
* aptitude
* sudo
* xserver-xorg-core
* xserver-xorg-video-vesa
* xserver-xorg-video-fbdev
* xserver-xorg-input-all
* pulseaudio
* alsa-utils
* mesa-utils
* retroarch
* xinit
* netsurf

[^1]: The list is partial, need to double check it yet.


## installation and test:

Just download in your home the file rocknplay.sh and .dialogrc. Run the 
script through ```./rocknplay.sh``` and test it. Currently it doesn't 
do much things because them must be implemented yet.


## Things that must be still implemented:

* fix the proportion for 1920x1080 resolution;[^2]
* decide if the script runs in the user home or ```/usr/local/bin```;
* add gamepad support;
* store the user selections somewhere;
* handle wifi connection;
* enable/disable ssh server;
* general improvements.

[^2]: It can considered almost solved since the moment you can get the console size through ```stty size```

## To do list:

* create a wiki section;
* install/uninstall the Debian games through a checklist;
* add a volume control with a gaugebox;
* add the FBI package for displaying photos in framebuffer;
* add MPV (or VLC) to play videos and music in framebuffer;
* anything else?


## Consideration

Due my limited knowledge in bash (and generally in coding) and my lack 
of sparetime I can't assure a regular development, anyway thanks for 
reading untill here!


###### FOOTNOTES